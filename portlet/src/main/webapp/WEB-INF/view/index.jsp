<%@ taglib prefix="s" uri="/struts-tags" %>
<H2>Bienvenido a los ejemplos portlet de Struts</H2>
<p/>
Aqu&iacute; encontrar&aacute; ejemplos de lo que es posible con el marco de integraci&oacute;n de Struts Portlet.
<ul>
<li><a href="<s:url action="formExample"/>">A simple form</a></li>
<li><a href="<s:url action="formExamplePrg" method="input"/>">Form example with proper PRG</a></li>
<li><a href="<s:url action="formExampleModelDriven" method="input"/>">Model driven example</a>/li>
<li><a href="<s:url action="validationExample"/>">Validation</a></li>
<li><a href="<s:url action="tokenExample"/>">Token</a></li>
<li><a href="<s:url action="springExample"/>">Spring integration</a></li>
<li><a href="<s:url action="fileUpload" method="input"/>">File upload</a></li>
<li><a href="<s:url action="freeMarkerExample"/>">FreeMarker</a></li>
<li><a href="<s:url action="velocityHelloWorld"/>">Velocity</a></li>
<li><a href="<s:url action="index" namespace="/tiles"/>">Form Example Validation with Tiles</a></li>
<li><a href="<s:url action="index" portletMode="edit"/>">Go to edit mode and see what's there</a></li>
<li><a href="<s:url action="index" portletMode="help"/>">Go to help mode and see what's there</a></li>
</ul>
