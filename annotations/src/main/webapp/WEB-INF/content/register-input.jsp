<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Basic Struts 2 Application - Register</title>
</head>

<body>
<h3>Registro en el formulario.</h3>

<s:form action="register">

    <s:textfield name="personBean.firstName" label="Nombre"/>
    <s:textfield name="personBean.lastName" label="Apellido"/>
    <s:textfield name="personBean.email" label="Correo"/>
    <s:textfield name="personBean.age" label="Edad"/>

    <s:submit/>

</s:form>

</body>
</html>