<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Anotaciones en Struts 2</title>
</head>
<body>
<h1>Bienvenido a Struts 2!</h1>

<p><a href="<s:url action='hello'  />" >Saludar.</a></p>
<p><a href="<s:url action='register-input'  />" > Registrarse.</a></p>

</body>
</html>