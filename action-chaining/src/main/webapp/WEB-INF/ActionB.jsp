<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Action B</title>
</head>

<body>

<h2>Valor desde Action A:</h2>
<p><s:property value="name"/></p>
<s:a action="index">Regreso a home</s:a>

</body>
</html>
