<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Hola mundo en Struts2</title>
  </head>
  <body>
    <h2>Producto</h2>
    <p>Id: <s:property value="producto.id"/></p>
    <p>Nombre: <s:property value="producto.nombre"/></p>
    <p>Precio: <s:property value="producto.precio"/></p>
    <p>Fecha: <s:property value="producto.fecha"/></p>
    <p>Disponible: <s:property value="producto.disponible"/></p>
    <p><a href="<s:url action='index'/>">Regreso</a></p>
  </body>
</html>