<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Aplicacion Struts2</title>
  </head>
  <body>
    <h1>Bienvenido a mi aplicaci&oacute;n</h1>
    <p><a href="<s:url action='hello'/>">Saludar</a></p>
    <p><a href="<s:url action='producto'/>">Ver producto</a></p>
  </body>
</html>
