<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Hola mundo en Struts2</title>
  </head>
  <body>
    <h2><s:property value="messageStore.message" /></h2>
    <p><a href="<s:url action='index'/>">Regreso</a></p>
  </body>
</html>
