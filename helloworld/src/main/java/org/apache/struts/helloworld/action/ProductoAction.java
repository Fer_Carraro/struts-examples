package org.apache.struts.helloworld.action;

import org.apache.struts.helloworld.model.Producto;
import com.opensymphony.xwork2.ActionSupport;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class ProductoAction extends ActionSupport {

    private static final long serialVersionUID = 1L; 

    private Producto producto;

    
    public String execute() {
        producto = new Producto() ;
        producto.setId(3);
        producto.setNombre("Jabones fresita");
        producto.setPrecio(22.21);
        producto.setDisponible(true);
        producto.setFecha(LocalDateTime.now());
        return SUCCESS;
    }


    public Producto getProducto(){
        return producto;
    }

    

}
