package org.apache.struts.helloworld.model;

import java.time.LocalDateTime;

public class Producto{
    private int id;
    private String nombre;
    private LocalDateTime fecha;
    private double precio;
    private boolean disponible;


    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public LocalDateTime getFecha(){
        return fecha;
    }

    public void setFecha(LocalDateTime fecha){
        this.fecha = fecha;
    }

    public double getPrecio(){
        return precio;
    }

    public void setPrecio(double precio){
        this.precio = precio;
    }

    public boolean isDisponible(){
        return disponible;
    }    

    public void setDisponible(boolean disponible){
        this.disponible = disponible;
    }

    @Override
    public String toString(){
        String estaDisponible = this.disponible? "Esta disponible" : "No esta disponible";
        return "Producto{ id: "+this.id+", nombre: "
        +this.nombre+", fecha: "+this.fecha+", disponible: "+estaDisponible+", precio: "+this.precio+" }";
    }

}