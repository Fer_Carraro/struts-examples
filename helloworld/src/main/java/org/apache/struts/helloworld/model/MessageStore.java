package org.apache.struts.helloworld.model;

/**
 * Model class that stores a message.
 * @author Bruce Phillips
 *
 */
public class MessageStore {
    
    private String message;
    
    public MessageStore() {
        message = "Fernando Carraro Aguirre";
    }

    public String getMessage() {
        return new String("Hola, ").concat(message);
    }

}
