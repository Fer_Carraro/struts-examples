Struts2 en ejemplos
---------------

[![Build Status](https://ci-builds.apache.org/buildStatus/icon?job=Struts%2FStruts-examples-JDK8-master)](https://ci-builds.apache.org/job/Struts/job/Struts-examples-JDK8-master/)
[![Build Status @ Travis](https://travis-ci.org/apache/struts-examples.svg?branch=master)](https://travis-ci.org/apache/struts-examples)
[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)


Este proyecto de varios módulos de Maven contiene todas las aplicaciones de ejemplo de Struts 2 que forman parte de los tutoriales Getting Started Struts 2 en http://struts.apache.org.

Para construir todas las aplicaciones de ejemplo, ejecute el comando Maven:

```
mvn -e clean package
```

En la carpeta raíz del proyecto, Maven construirá cada módulo y creará un archivo `.war` en la subcarpeta de destino de cada módulo.

Luego puede copiar los archivos `.war` a su contenedor Servlet (por ejemplo, Tomcat, Jetty, GlassFish, etc.).

Hay un archivo README en cada módulo con instrucciones y la URL para ver esa aplicación.
