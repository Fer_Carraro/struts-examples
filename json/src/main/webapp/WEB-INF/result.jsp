<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
  <title>JSON Resultados</title>
</head>

<body>
<ul>
  <li>
    contador: <s:property value="bean.counter"/>
  </li>
  <s:iterator value="bean.names" var="name" status="idx">
  <li>
    nombre<s:property value="#idx"/>: <s:property value="#name"/>
  </li>
  </s:iterator>
</ul>
<a href="index.action">Regresar</a>

</body>
</html>
